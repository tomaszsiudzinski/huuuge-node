import path from 'path'
import dotenv from 'dotenv'
dotenv.config({
  // path: path.join(__dirname, '..')
})
import express, { RequestHandler, Request, Response, NextFunction } from 'express'
import routes from './routes';
import { NotFoundError } from "./services/errors";
console.log(path.join(__dirname, '..'))

const app = express();

// express.urlencoded()
// express.static

app.use(express.json({
  // limit:'100kb',
}))

app.use(routes);

app.use((error: any, req: Request, res: Response, next: NextFunction) => {
  if (error instanceof NotFoundError) {
    res.sendStatus(404)
  }
  next(error)
})

app.listen(parseInt(process.env.PORT!), process.env.HOST!, () => {
  console.log(`Listening on http://${process.env.HOST}:${process.env.PORT}/`)
});
