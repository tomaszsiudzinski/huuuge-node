import { UsersService } from "./users";
import { WishlistService } from './wishlist';
import { CouponsService } from "./coupons";

export const usersService = new UsersService()
export const wishlistService = new WishlistService()
export const couponsService = new CouponsService()

