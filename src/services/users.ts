import { NotFoundError } from "./errors";
import { createHash } from 'crypto'

interface User {
  id: string;
  name: string;
  password?: string;
}
type UserCreateRequestDTO = User
type UserUpdateRequestDTO = User

const mockUsers: User[] = [
  { id: '1', name: "User 1" },
  { id: '2', name: "User 2" },
];

export class UsersService {
  private users = mockUsers;

  async updateUser(userDTO: UserUpdateRequestDTO) {
    const index = await this.userExists(userDTO.id);
    if (index === -1) { throw 'User doesnt exist' }

    this.hashNewPassword(userDTO);
    
    this.users[index] = userDTO;
    return userDTO;
  }

  async saveUser(body: any) {
    const index = await this.userExists(body.id);
    if (index !== -1) { throw 'User already exists' }

    this.users[index] = body;
    return body;
  }

  async getUsers() {
    return mockUsers.map(({ password, ...data }) => (
      // { ...data }
      data
    ))
  }

  async getUser(user_id: string) {
    const user = this.users.find((u) => u.id == (user_id))
    if (!user) { throw new NotFoundError('User Not Found') }
    return user
  }

  async userExists(user_id: string) {
    return this.users.findIndex((u) => u.id == (user_id))
  }



  private hashNewPassword(userDTO: User) {
    if (userDTO.password) {

      const md5 = createHash('md5');
      md5.update(userDTO.password);
      userDTO.password = md5.digest('hex');
    }
  }
}

