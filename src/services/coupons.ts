

const mockCoupons = [
  { id: 1, code: 'coupon 1' },
  { id: 2, code: 'coupon 2' },
];


export class CouponsService {
  async addCoupon(body: { id: number; code: string; }) {
    mockCoupons.push(body)
  }

  private coupons = mockCoupons;

  async getCoupons() {
    return this.coupons;
   }

   async getCoupon(coupon_id: string) {
    return this.coupons.find(
      (c) => c.id == parseInt(coupon_id, 10)
    )
  }

  async couponExists(coupon_id: string) {
    throw new Error("Method not implemented.");
  }

}

