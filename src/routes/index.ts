import { Router } from "express";
import { categoriesRoutes } from './categories'
import { productsRoutes } from "./products";
import { usersRoutes } from "./users";
import { wishlistRoutes } from './wishlist';
import { paymentsRoutes } from "./payments";
import { reviewsRoutes } from "./reviews";
import { addressesRoutes } from "./addresses";
import { couponsRoutes } from "./coupons";
import { shoppingCardRoutes } from './shoppingCart';
import { ordersRoutes } from "./orders";

const routes = Router()
  .use("/shopping-card", shoppingCardRoutes)
  .use("/users", usersRoutes)
  .use("/products", productsRoutes)
  .use('/reviews', reviewsRoutes)
  .use('/wishlist', wishlistRoutes)
  .use("/payments", paymentsRoutes)
  .use("/reviews", reviewsRoutes)
  .use("/categories", categoriesRoutes)
  .use('/addresses', addressesRoutes)
  .use('/coupons', couponsRoutes)
  .use("/orders", ordersRoutes)


export default routes;
