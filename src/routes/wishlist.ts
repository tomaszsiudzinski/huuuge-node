import { Router } from 'express';
import { wishlistService } from '../services'



export const wishlistRoutes = Router()
    .post('/', async (req, res) => {
        const data = req.body;
        console.log(data);
        await wishlistService.save({productId: data.product});

        res.redirect('/wishlist/');
    })
    .get('/:id', async (req, res) => {
        const productId = parseInt(req.params.id);
        const result = await wishlistService.getWishlistElement(productId);
        res.json(result);
    })
    .delete('/:id', async (req, res) => {
        const productId = parseInt(req.params.id);
        await wishlistService.remove(productId);
        res.redirect('/wishlist/');
    })
    .get('/', async (req, res) => {
        const list = await wishlistService.getAll();
        res.json(list);
    })