// import { Router } from "express";
import Router from "express-promise-router";
import { usersService } from '../services'
// import url from 'url'
// url.parse(req.url).path

export const usersRoutes = Router()
  // .param(":user_id", (req, res, next) => { })

  .get("/", async (req, res) => {
    
  
    res.json(await usersService.getUsers());
  })

  .get("/:user_id", async (req, res, next) => {
    try {
      const { user_id } = req.params;
      res.json(await usersService.getUser(user_id));
    } catch (err) { next(err) }
  })

  .post("/", async (req, res, next) => {
    const body = req.body;

    const exist = await usersService.userExists(body.id);

    if (exist !== -1) {
      res.sendStatus(409).json({ message: 'User already exists' })
    }
    const result = await usersService.saveUser(body);

    res.json(result);
  })

  .put("/:user_id", async (req, res, next) => {
    const body = req.body;
    const existIndex = await usersService.userExists(body.id);

    if (existIndex == -1) {
      next(new Error('User doesn`t exists'))
    } else {
      const result = await usersService.updateUser(body);
      res.send(result);
    }
  });
