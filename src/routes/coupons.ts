import { Router } from "express";
import { couponsService } from '../services'

export const couponsRoutes = Router()

  .get('/', async (req, res) => {
    const result = await couponsService.getCoupons()
    res.json(result)
  })

  .post('/', async (req, res, next) => {
    const coupon = req.body;
    await couponsService.addCoupon(coupon);
  })

  .get('/:coupon_id', async (req, res) => {
    const { coupon_id } = req.params;
    const result = await couponsService.getCoupon(coupon_id)
    res.json(result)
  })