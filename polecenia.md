git clone https://bitbucket.org/ev45ive/huuuge-node.git
cd huuuge-node
npm i
npm run start:dev

# Updates to fork
git remote add upstream https://bitbucket.org/ev45ive/huuuge-node.git
git pull -f upstream master

# Init
npm init -y


# Update node
curl https://www.npmjs.com/install.sh | sh

# NVM
nvm ls-remote
nvm install <version> 
nvm use <version>

# Typescript
npm i -g typescript
tsc --init

npm i @types/node

# Watch
npm i nodemon
tsc --watch ./src
nodemon -w ./dist --respawn ./dist/index.js

npm i ts-node ts-node-dev

# Modules
npm i express @types/express